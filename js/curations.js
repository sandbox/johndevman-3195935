(function (Drupal, Sortable) {

  function createSortable(selector, context, sortable) {
    const el = context.querySelector(selector);

    Sortable.create(el, {
      draggable: '.js-curations-document',
      ghostClass: 'ui-state-drop',
      handle: '.js-curations-document-handle',
      group: 'curations',
      sort: sortable,
    });
  }

  Drupal.behaviors.curationsPromotedDrag = {
    attach: function attach(context) {
      createSortable('.js-curations-promoted', context, true);
      createSortable('.js-curations-documents', context, false);
    }
  };

})(Drupal, Sortable);
