<?php

namespace Drupal\curations\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\ResultSetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Curates search result.
 *
 * @SearchApiProcessor(
 *   id = "curation",
 *   label = @Translation("Curation"),
 *   description = @Translation("Curates search result"),
 *   stages = {
 *     "postprocess_query" = 0,
 *   }
 * )
 */
class Curation extends ProcessorPluginBase implements PluginFormInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->setEntityTypeManager($container->get('entity_type.manager'));

    return $processor;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager ?: \Drupal::entityTypeManager();
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The new entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) {
    $query = $results->getQuery();
    $keys = $query->getOriginalKeys();

    if (is_array($keys)) {
      $keys = implode(' ', $keys);
    }

    $storage = $this->getEntityTypeManager()->getStorage('curation');

    $curations = $storage->loadByProperties([
      'queries' => $keys,
      'index' => $query->getIndex()->id(),
    ]);

    if (empty($curations)) {
      return $results;
    }

    $curation = reset($curations);

    $promoted_ids = [];

    foreach ($curation->promoted as $delta => $promoted) {
      $promoted_ids[$promoted->value] = $delta;
    }

    $result_items = $results->getResultItems();

    $promoted_items = [];

    foreach ($result_items as $result_item_delta => $result_item) {
      $result_item_id = $result_item->getId();

      if (isset($promoted_ids[$result_item_id])) {
        $delta = $promoted_ids[$result_item_id];
        $promoted_items[$delta] = $result_item;

        unset($result_items[$result_item_delta]);
      }
    }

    array_unshift($result_items, ...$promoted_items);

    $results->setResultItems($result_items);

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
