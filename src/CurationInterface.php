<?php

namespace Drupal\curations;

use Drupal\Core\Entity\ContentEntityInterface;

interface CurationInterface extends ContentEntityInterface {}
