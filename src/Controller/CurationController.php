<?php

namespace Drupal\curations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\curations\Entity\Curation;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Query\Query;

/**
 * @todo Use entity form
 * @todo Move to a render element?
 */
class CurationController extends ControllerBase {

  public function manage() {

    $build = [
      '#type' => 'container',
    ];

    $promoted = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['js-curations-promoted'],
      ],
      'title' => ['#markup' => '<h2>Promoted documents</h2>'],
    ];

    $curation = Curation::load(1);

    $promoted_documents = $curation->promoted;

    $promoted_document_ids = [];

    foreach ($promoted_documents as $promoted_document) {
      $promoted_document_ids[] = $promoted_document->value;
      $promoted[] = ['#theme' => 'curations_document', '#value' => $promoted_document->value];
    }

    $build['promoted'] = $promoted;


    // Documents...

    /** @var \Drupal\search_api\IndexInterface $index */
    $index = Index::load('default_index');

    $query = Query::create($index);
    $query->keys('Page');

    $result_set = $query->execute();

    $documents = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['js-curations-documents'],
      ],
      'title' => ['#markup' => '<h2>Documents</h2>'],
    ];

    foreach ($result_set->getResultItems() as $result_item) {
      if (in_array($result_item->getId(), $promoted_document_ids)) {
        continue;
      }
      $documents[] = ['#theme' => 'curations_document', '#value' => $result_item->getId()];
    }

    $build['documents'] = $documents;

    $build['#attached']['library'][] = 'curations/drupal.curations';

    // :)
    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
