<?php

namespace Drupal\curations\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\curations\CurationInterface;

/**
 * Defines the curation entity class.
 *
 * @ContentEntityType(
 *   id = "curation",
 *   label = @Translation("Curation"),
 *   label_singular = @Translation("curation"),
 *   label_plural = @Translation("curations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count curation",
 *     plural = "@count curations"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\curations\CurationListBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\curations\Form\CurationForm",
 *       "add" = "Drupal\curations\Form\CurationForm",
 *       "edit" = "Drupal\curations\Form\CurationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "curation",
 *   data_table = "curation_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer curations",
 *   links = {
 *     "add-form" = "/curations/add",
 *     "canonical" = "/curations/{curation}/edit",
 *     "collection" = "/admin/content/curations",
 *     "delete-form" = "/curations/{curation}/delete",
 *     "delete-multiple-form" = "/curations/delete",
 *     "edit-form" = "/curations/{curation}/edit",
 *   }
 * )
 */
class Curation extends ContentEntityBase implements CurationInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['index'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Index'))
      ->setRequired(TRUE)
      ->setDescription(t('Search index'))
      ->setSetting('target_type', 'search_api_index')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['queries'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Queries'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['promoted'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Promoted'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['hidden'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hidden'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
